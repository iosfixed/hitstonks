import numpy as np

# Целевая функция получает на вход агреггированные ряды after и before

def delta(agg_before, agg_after):
    return agg_after - agg_before


def strict_agg_after(agg_before, agg_after):
    return agg_after


def delta_sign(agg_before, agg_after):
    return np.sign(agg_after - agg_before)

def border_fn(agg_before, agg_after):
    border = 0.0029644479072929455
    value = (np.mean(agg_after) - np.mean(agg_before)) / np.mean(agg_before)
    if(value > border):
        return 2
    elif(value < border and value > -border):
        return 1
    else:
        return 0

default = border_fn
