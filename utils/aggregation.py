import numpy as np


# Агреггирующая функция получает на вход ряд и возвращает число

def mean(series):
    return np.mean(series)


def harmonic_mean(series):
    return 1 / np.mean(1 / series)


def strict(series):
    return series


def angle(series):
    x, y = list(zip(*enumerate(series)))
    slope_tg, intercept = np.polyfit(x, y, deg=1)

    slope_angle = np.arctan(slope_tg)

    return slope_angle


default = mean
