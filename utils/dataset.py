import random

import pandas as pd
import numpy as np

from tqdm import tqdm

from datetime import timedelta
from consts import TWEETS_DIR, DEF_SINCE, DEF_SRC, DEF_KEY, DEF_TILL, DEF_ASSET, FINAM_DIR
from utils import aggregation as agg, target
from utils.tweet_preprocessors import preprocess_tweet

from torch.utils.data import Dataset

def first_step(min_back=120,
               min_forward=120,
               asset=DEF_ASSET,
               keywords=DEF_KEY,
               sources=DEF_SRC,
               since=DEF_SINCE,
               until=DEF_TILL
               ):

    # Загружаем твиты:
    if not isinstance(sources, list):
        sources = [sources]

    tweets_df = pd.DataFrame()
    for src_name in sources:
        csv_path = TWEETS_DIR / f'{src_name.lower()}.csv'
        df = pd.read_csv(csv_path)

        tweets_df = tweets_df.append(df)

    tweets_df.tweet = tweets_df.tweet.apply(preprocess_tweet)
    tweets_df = tweets_df[tweets_df.date.between(since, until)]

    if keywords:
        mask = np.zeros_like(tweets_df.tweet)
        for keyword in keywords:
            regex = r'\s' + keyword + '[\s\.]'
            mask = mask | tweets_df.tweet.str.contains(regex)

        tweets_df = tweets_df[mask]

    tweets_df.date = pd.to_datetime(tweets_df.date)

    # Загружаем котировки
    prices_df = pd.read_csv(FINAM_DIR / f'{asset}.csv')
    prices_df['<DATETIME>'] = pd.to_datetime(prices_df['<DATETIME>'])
    prices_df = prices_df[prices_df['<DATETIME>'].between(since, until)]

    back_delta = timedelta(minutes=min_back)
    forward_delta = timedelta(minutes=min_forward)

    # Собираем твиты, ряды котировок до и после твита
    data = []
    for idx, row in tqdm(tweets_df.iterrows(), desc='First step', total=tweets_df.shape[0]):
        tweet = row.tweet
        tweet_timestamp = row.date

        before_tweet = prices_df[prices_df['<DATETIME>'].between(tweet_timestamp - back_delta, tweet_timestamp)]
        after_tweet = prices_df[prices_df['<DATETIME>'].between(tweet_timestamp, tweet_timestamp + forward_delta)]

        before_values = before_tweet['<CLOSE>'].values
        after_values = after_tweet['<CLOSE>'].values

        if len(before_values) >= 1 and len(after_values) >= 1:
            data.append((tweet, before_values, after_values))

    return data

def first_step_full_data(min_back=120,
               min_forward=120,
               asset=DEF_ASSET,
               keywords=DEF_KEY,
               sources=DEF_SRC,
               since=DEF_SINCE,
               until=DEF_TILL
               ):

    # Загружаем твиты:
    if not isinstance(sources, list):
        sources = [sources]

    tweets_df = pd.DataFrame()
    for src_name in sources:
        csv_path = TWEETS_DIR / f'{src_name.lower()}.csv'
        df = pd.read_csv(csv_path)

        tweets_df = tweets_df.append(df)

    tweets_df.tweet = tweets_df.tweet.apply(preprocess_tweet)
    tweets_df = tweets_df[tweets_df.date.between(since, until)]

    tweets_df.date = pd.to_datetime(tweets_df.date)

    # Загружаем котировки
    prices_df = pd.read_csv(FINAM_DIR / f'{asset}.csv')
    prices_df['<DATETIME>'] = pd.to_datetime(prices_df['<DATETIME>'])

    back_delta = timedelta(minutes=min_back)
    forward_delta = timedelta(minutes=min_forward)

    # Собираем твиты, ряды котировок до и после твита
    data = []
    for idx, row in tweets_df.iterrows():
        tweet = row.tweet
        tweet_timestamp = row.date

        before_tweet = prices_df[prices_df['<DATETIME>'].between(tweet_timestamp - back_delta, tweet_timestamp)]
        after_tweet = prices_df[prices_df['<DATETIME>'].between(tweet_timestamp, tweet_timestamp + forward_delta)]

        before_values = before_tweet['<CLOSE>'].values
        after_values = after_tweet['<CLOSE>'].values

        if len(before_values) >= 2 and len(after_values) >= 2:
            data.append((tweet, before_values, after_values))

    return data

def second_step(data,
                tweet_processor=lambda x: x,
                before_agg_fn=agg.default,
                after_agg_fn=agg.default,
                target_fn=target.default):

    train_data = []
    for tweet, before, after in tqdm(data, desc='Second step'):
        processed_tweet = tweet_processor(tweet)
        agg_before = before_agg_fn(before)
        agg_after = after_agg_fn(after)

        target = target_fn(agg_before, agg_after)

        X = (processed_tweet, before, agg_before)
        y = target

        train_data.append((X, y))

    return train_data


class DatasetMkI(Dataset):
    def __init__(self, train_data, shuffle=True):
        X, self.y = list(zip(*train_data))
        self.tweets, self.before, self.agg_before = list(zip(*X))
        self.index_map = list(range(len(self)))
        self.shuffle = shuffle

    def __len__(self):
        return len(self.y)

    def _shuffle(self):
        random.shuffle(self.index_map)

    def __getitem__(self, item):
        shuffled_item = self.index_map[item]

        X = self.tweets[shuffled_item], self.before[shuffled_item], self.agg_before[shuffled_item]
        y = self.y[shuffled_item]

        if self.shuffle and item == len(self) - 1:
            self._shuffle()

        return X, y


if __name__ == "__main__":
    first_step()