import re
from num2words import num2words

SIGN_REPLACE_DICT = {}
SIGN_REPLACE_DICT['’'] = "'"
SIGN_REPLACE_DICT['$'] = ' dollar '
SIGN_REPLACE_DICT['%'] = ' percent '
SIGN_REPLACE_DICT['@'] = ' at '
SIGN_REPLACE_DICT['#'] = ' hashtag '
SIGN_REPLACE_DICT['?'] = ' question '

URL_REGEX = r"(?i)\b((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'\".,<>?«»“”‘’]))"


def numbers2words(tweet):
    tweet = re.sub(r'(\d+)\s(\d+)', r'\g<1>\g<2>', tweet)
    # tweet = re.sub(r'(\d+)\.(\d+)', r'\g<1>\g<2>', tweet)

    replace_dict = {}
    for number in re.findall(r'\d+', tweet):
        try:
            replace_dict[number] = num2words(number)
        except:
            print(number)

    for number, replacement in replace_dict.items():
        tweet = tweet.replace(number, replacement)

    return tweet

def replace_signs(tweet):
    for sign, replacement in SIGN_REPLACE_DICT.items():
        tweet = tweet.replace(sign, replacement)

    return tweet

def cut_links(tweet):
    tweet = re.sub(URL_REGEX, '', tweet)

    return tweet

def preprocess_tweet(tweet):
    tweet = tweet.lower()
    tweet = cut_links(tweet)
    tweet = replace_signs(tweet)
    tweet = numbers2words(tweet)

    for _ in range(5):
        tweet = tweet.replace('  ', ' ')

    return tweet

def standardize_text(df, text_field):
    df[text_field] = df[text_field].str.replace(r"http\S+", "")
    df[text_field] = df[text_field].str.replace(r"http", "")
    df[text_field] = df[text_field].str.replace(r"@\S+", "")
    df[text_field] = df[text_field].str.replace(r"[^A-Za-z0-9\'\`]", " ")
    df[text_field] = df[text_field].str.replace(r"@", "at")
    df[text_field] = df[text_field].str.replace(r'\'|\"', '')
    df[text_field] = df[text_field].str.replace(r'\b[^\W\d_]{1,2}\b', '')
    df[text_field] = df[text_field].str.lower()
    return df

def labelize_text_ug(text_series, label):
    result = []
    prefix = label
    for index, text in zip(text_series.index, text_series):
        result.append(TaggedDocument(text.split(), [prefix + '_%s' % index]))
    return result