#https://github.com/RaRe-Technologies/gensim-data#models

import numpy as np

import gensim.downloader as api
from gensim.utils import simple_preprocess, simple_tokenize, lemmatize

class w2vGoogleNews300:
    def __init__(self):
        self.wv = api.load('word2vec-google-news-300')
        self.tokenizer = simple_tokenize

    def __call__(self, tweet):
        tokens = self.tokenizer(tweet)
        vectors = []

        for token in tokens:
            if token in self.wv:
                vectors.append(self.wv[token])

        if vectors:
            return np.vstack(vectors)

        return None

class EmbeddingsGloVe:
    def __init__(self, path="glove-twitter-100"):
        self.wv = api.load(path)
        self.tokenizer = simple_tokenize

    def __call__(self, tweet):
        tokens = self.tokenizer(tweet)
        vectors = []

        for token in tokens:
            if token in self.wv:
                vectors.append(self.wv[token])

        if vectors:
            return np.vstack(vectors)

        return None

class EmbeddingsFastText:
    def __init__(self, path="fasttext-wiki-news-subwords-300"):
        self.wv = api.load(path)
        self.tokenizer = simple_tokenize

    def __call__(self, tweet):
        tokens = self.tokenizer(tweet)
        vectors = []

        for token in tokens:
            if token in self.wv:
                vectors.append(self.wv[token])

        if vectors:
            return np.vstack(vectors)

        return None
