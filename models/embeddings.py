import multiprocessing
from gensim.models.word2vec import Word2Vec
from gensim.models import KeyedVectors

from sklearn import utils as skutils
from tqdm import tqdm

import numpy as np

class Embeddings1:
    def __init__(self):
        cores = multiprocessing.cpu_count()
        self.model_ug_cbow = Word2Vec(
            sg=0, size=100, negative=5, window=2, min_count=2, workers=cores, alpha=0.065, min_alpha=0.065)
        self.model_ug_sg = Word2Vec(sg=1, size=100, negative=5, window=2,
                                    min_count=2, workers=cores, alpha=0.065, min_alpha=0.065)

    def learn(self, all_x_w2v):
        self.model_ug_cbow.build_vocab([x.words for x in tqdm(all_x_w2v)])
        for epoch in range(10):
            self.model_ug_cbow.train(skutils.shuffle(
                [x.words for x in tqdm(all_x_w2v)]), total_examples=len(all_x_w2v), epochs=1)
            self.model_ug_cbow.alpha -= 0.002
            self.model_ug_cbow.min_alpha = self.model_ug_cbow.alpha
        self.model_ug_sg.build_vocab([x.words for x in tqdm(all_x_w2v)])
        for epoch in range(10):
            self.model_ug_sg.train(skutils.shuffle([x.words for x in tqdm(
                all_x_w2v)]), total_examples=len(all_x_w2v), epochs=1)
            self.model_ug_sg.alpha -= 0.002
            self.model_ug_sg.min_alpha = self.model_ug_sg.alpha

    def get_index(self):
        embeddings_index = {}
        for w in self.model_ug_cbow.wv.vocab.keys():
            embeddings_index[w] = np.append(
                self.model_ug_cbow.wv[w], self.model_ug_sg.wv[w])
        return embeddings_index

    def save(self):
        self.model_ug_cbow.save('w2v_model_ug_cbow_tonality.word2vec')
        self.model_ug_sg.save('w2v_model_ug_sg_tonality.word2vec')

    def load(self):
        self.model_ug_cbow = KeyedVectors.load(
            'w2v_model_ug_cbow_tonality.word2vec')
        self.model_ug_sg = KeyedVectors.load(
            'w2v_model_ug_sg_tonality.word2vec')