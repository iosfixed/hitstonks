import torch
from keras.models import Model

from keras.layers import Input, Dense, concatenate, Activation
from keras.layers import Conv1D, GlobalMaxPooling1D
from keras.layers import Dropout
from keras.layers.embeddings import Embedding

from torch import nn

class TorchMkI(nn.Module):
    def __init__(self):
        super(TorchMkI, self).__init__()
        self.tweet_gru = nn.GRU(input_size=100, hidden_size=128, num_layers=1, batch_first=True)

        self.fc1 = nn.Linear(128, 64)
        self.fc2 = nn.Linear(64, 32)
        self.fc3 = nn.Linear(32, 8)
        self.fc4 = nn.Linear(8, 3)

        self.agg_before_fc = nn.Linear(1, 8)

        self.activation = nn.Tanh()

    def forward(self, tweet, agg_before):
        tweet_x = self.tweet_gru(tweet)

        output, state = tweet_x

        x = state.view(1, -1)
        x = self.fc1(x)
        x = self.activation(x)
        x = self.fc2(x)
        x = self.activation(x)
        x = self.fc3(x)
        x = self.activation(x)

        # agg_before_x = self.agg_before_fc(agg_before[None, :])
        # x = torch.cat([x, agg_before_x], dim=1)
        x = self.fc4(x)

        return x

def MkI(embedding_matrix):
    text_input = Input(shape=(45,), dtype='int32')
    text_encoder = Embedding(100000, 200, weights=[
        embedding_matrix], input_length=45, trainable=True)(text_input)

    bigram_branch = Conv1D(filters=100, kernel_size=2, padding='valid',
                           activation='relu', strides=1)(text_encoder)
    bigram_branch = GlobalMaxPooling1D()(bigram_branch)
    trigram_branch = Conv1D(filters=100, kernel_size=3,
                            padding='valid', activation='relu', strides=1)(text_encoder)
    trigram_branch = GlobalMaxPooling1D()(trigram_branch)
    fourgram_branch = Conv1D(filters=100, kernel_size=4,
                             padding='valid', activation='relu', strides=1)(text_encoder)
    fourgram_branch = GlobalMaxPooling1D()(fourgram_branch)
    merged = concatenate(
        [bigram_branch, trigram_branch, fourgram_branch], axis=1)

    merged = Dense(256, activation='relu')(merged)
    merged = Dropout(0.2)(merged)
    merged = Dense(1)(merged)
    output = Activation('tanh')(merged)
    model = Model(inputs=[text_input], outputs=[output])
    model.compile(loss='mse',
                  optimizer='adam',
                  metrics=['accuracy'])
    model.summary()
    return model
