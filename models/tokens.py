from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences

import numpy as np

import pickle

class Tokens1:
    def __init__(self):
        self.tokenizer = Tokenizer(num_words=100000)
        self.x_train_seq = None
        self.x_val_seq = None

    def hand(self, x):
        sequences = self.tokenizer.texts_to_sequences(x)
        return pad_sequences(sequences, maxlen=45)

    def learn(self, x_train):
        self.tokenizer.fit_on_texts(x_train)

    def get_embedding_matrix(self, embeddings_index):
        num_words = 100000
        embedding_matrix = np.zeros((num_words, 200))
        for word, i in self.tokenizer.word_index.items():
            if i >= num_words:
                continue
            embedding_vector = embeddings_index.get(word)
            if embedding_vector is not None:
                embedding_matrix[i] = embedding_vector
        return embedding_matrix

    def save(self):
        with open('tokenizer.pickle', 'wb') as handle:
            pickle.dump(self.tokenizer, handle,
                        protocol=pickle.HIGHEST_PROTOCOL)

    def load(self):
        with open('tokenizer.pickle', 'rb') as handle:
            self.tokenizer = pickle.load(handle)
