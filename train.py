from pathlib import Path
import os, shutil
import numpy as np

import pickle

import torch
from mxnet.gluon.data import DataLoader
from torch import nn
from tqdm import tqdm
from sklearn.metrics import confusion_matrix
from models.models import TorchMkI
from utils.dataset import DatasetMkI

dataset_root = Path('./datasets')
NAME = 'business_wsj_brent_2017_19_glove100'

datasets = dict(train=None, val=None)
for fold in datasets:
    src = dataset_root / f'{NAME}_{fold}.pickle'
    with open(src, 'rb') as f:
        obj = pickle.load(f)

    datasets[fold] = obj

model = TorchMkI().cuda()

optim = torch.optim.Adam(model.parameters(), lr=0.001)
scheduler = torch.optim.lr_scheduler.ExponentialLR(optim, 0.997)

loss_fn = nn.CrossEntropyLoss(reduction='none')

EPOCHES = 5000
BATCH_SIZE = 32

for epoch in range(EPOCHES):
    print(f'Epoch ({epoch})')

    for phase in ['train', 'val']:
        if phase == 'train':
            model = model.train()  # Переводим модель в режим обучения (веса меняются)
            torch.set_grad_enabled(True)  # Сохраняем градиенты вычислительного графа

        if phase == 'val':
            model = model.eval()  # Переводим модель в режим исполнения (веса не меняются)
            torch.set_grad_enabled(False)  # Несохраняем градиенты вычислительного графа

        running_pred = []
        running_ans = []
        running_loss = []
        running_agg_before = []

        batch_pred = []
        batch_ans = []

        dataset = datasets[phase]
        for n, sample in enumerate(dataset):
            X, y = sample
            tweet, before, agg_before = X

            tweet = torch.Tensor(tweet).cuda()[None, ...]
            agg_before = torch.Tensor([agg_before]).cuda()

            y = torch.Tensor([y]).cuda()

            predictions = model(tweet, agg_before)

            running_pred.append(predictions)
            running_ans.append(y)
            running_agg_before.append(agg_before)

            batch_pred.append(predictions)
            batch_ans.append(y)

            if len(batch_pred) == BATCH_SIZE or n == len(dataset) - 1:
                pred = torch.cat(batch_pred)
                ans = torch.cat(batch_ans)

                W = torch.ones_like(ans)
                W[ans != 1] *= 5

                loss = W * loss_fn(pred, ans.long())
                loss = loss.mean()

                running_loss.append(loss.item())

                if phase == 'train':  # Если нужно поменять веса - пишем три волшебные комманды
                    optim.zero_grad()
                    loss.backward()
                    optim.step()  # На этом шаге изменяются веса модели

                batch_pred = []
                batch_ans = []

        running_pred = torch.cat(running_pred).detach().cpu().numpy()
        running_ans = torch.cat(running_ans).detach().cpu().numpy()
        running_agg_before = torch.cat(running_agg_before)

        category_pred = np.argmax(running_pred, axis=1)
        acc = np.mean((category_pred == running_ans))
        cm = confusion_matrix(running_ans, category_pred)

        loss = np.mean(running_loss)

        print(f'{phase} accuracy {acc.item():.2f} / loss {loss.item():.2e}')
        print(cm)
        scheduler.step()  # Делаем палку тоньше после обучающей эпохи