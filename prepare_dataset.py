from pathlib import Path
import os, shutil
import numpy as np

import pickle

from utils.dataset import first_step, second_step, DatasetMkI
from utils.target import border_fn
from utils.tweet_embedders import w2vGoogleNews300, EmbeddingsGloVe

root = Path('./datasets')

os.makedirs(root, exist_ok=True)


# First Step params
min_back = 120
min_forward = 120

asset = 'brent'
key = ['OPEC', 'oil', 'brent', 'petroleum', 'gasoline']
src = ['wsj', 'business']

since = '2017-01-01'
till = '2019-01-01'

# Second step params
tweet_processor = EmbeddingsGloVe()
agg_fn = np.mean
target_fn = border_fn

NAME = 'business_wsj_brent_2017_19_glove100'

data = first_step(min_back, min_forward, asset, key, src, since, till)
data = second_step(data, tweet_processor, agg_fn, agg_fn, target_fn)

VAL_PERCENTAGE = 0.2
train_samples = np.random.rand(len(data)) > VAL_PERCENTAGE
train_samples_idx = np.where(train_samples)[0]
val_samples_idx = np.where(~train_samples)[0]

train_data = [data[i] for i in train_samples_idx]
val_data = [data[i] for i in val_samples_idx]

train_dataset = DatasetMkI(train_data)
val_dataset = DatasetMkI(val_data)

print(f'Train size: {len(train_dataset)}')
print(f'Val size: {len(val_dataset)}')

for dataset, label in zip([train_dataset, val_dataset], 'train val'.split()):
    dst = root / f'{NAME}_{label}.pickle'
    with open(dst, 'wb') as f:
        pickle.dump(dataset, f)