from pathlib import Path

TWEETS_DIR = Path(__file__).parent / 'data' / 'tweets'
FINAM_DIR = Path(__file__).parent / 'data' / 'finam'

DEF_KEY = ['oil', 'OPEC', 'brent']
DEF_SRC = ['elonmusk']
DEF_SINCE = '2012-01-01'
DEF_TILL = '2017-01-01'
DEF_ASSET = 'tesla'
